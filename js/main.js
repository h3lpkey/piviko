var data_form = {
        'type': '',
        'name': '',
        'phone': '',
        'city': '',
        'sum': 1500000,
        'email': '',
        'test_q_1': '',
        'test_q_2': '',
        'test_q_3': '',
        'links': []
    },
    texts = {
        'first': 'Спасибо, в скором времени мы свяжемся с вами по ',
        'second': 'Спасибо, скоро мы вышлем вам Бизнес план франшизы Пив&Ко.',
        'third': 'Спасибо! скоро мы свяжемся с вами.',
        'test_answer_1': 'Похоже, вы Билл Гейтс. Мы очень хотим с вами сотрудничать! \n' +
            'Как с вами связаться?',
        'test_answer_2': 'Вы определенно понимаете в бизнесе, но на старте необходима поддержка. Мы готовы ее обеспечить!',
        'test_answer_number': 0
    },
    error_form = {
        'text': 'Заполните все поля',
        'status': false
    };

function resetDataForm() {
    data_form = {
        'type': '',
        'name': '',
        'phone': '',
        'city': '',
        'sum': 1500000,
        'email': '',
        'test_q_1': '',
        'test_q_2': '',
        'test_q_3': '',
        'links': []
    };
    $('.form-checkbox input').prop("checked", false);
    $('.form_phone').val('');
    $('.form_email').val('');
    $('.form_name').val('');
    $('.form_city').val('');
}

Array.prototype.remove = function (value) {
    var idx = this.indexOf(value);
    if (idx != -1) {
        return this.splice(idx, 1);
    }
    return false;
}

function validateEmail($email) {
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    return emailReg.test($email);
}

function openTab(numTub) {
    let $ring = $('.tab-link .ring'),
        $lastTab = $('.tab-open'),
        $needTab = $('.tab-' + [numTub]),
        $link = $ring[numTub];

    resetDataForm();
    data_form.type = numTub;

    $('.tab').each(function () {
        $(this).removeClass('tab-open').css('display', 'none');
    });

    $ring.removeClass('ring_gold');
    $($link).addClass('ring_gold');
    $($needTab).addClass('tab-open');

    if ($lastTab.length == 0) {
        $('.main').addClass('main-open');
        $('.left').addClass('left-open');
    }

    $('.tab-' + numTub).show();
    $("html,body").animate({scrollTop: 0}, 300);
}

function checkForm() {
    console.log(data_form);
    switch (data_form.type) {
        case 0:
            if (data_form.name &&
                data_form.phone &&
                data_form.city &&
                data_form.email &&
                data_form.sum &&
                data_form.links.length > 0) {

                $('#send_form').prop('disabled', false);
                let text = texts.first;
                data_form.links.forEach(function (item, idx, array) {
                    if (idx === array.length - 1) {
                        text += item + '.';
                    } else {
                        text += item + ', ';
                    }
                });
                text = text.replace(/.$/, ".");
                $('#text_success').text(text)
            } else {
                $('#send_form').prop('disabled', true)
            }
            break;
        case 1:
            if (data_form.name &&
                data_form.phone &&
                data_form.email &&
                data_form.links.length > 0) {
                $('#send_form_2').prop('disabled', false);
                $('#text_success').text(texts.second)
            } else {
                $('#send_form_2').prop('disabled', true)
            }
            break;
        case 2:
            if (data_form.test_q_1 != '' &&
                data_form.test_q_2 != '' &&
                data_form.test_q_3 != '') {
                $('#get-result-test').prop('disabled', false);
            } else {
                $('#get-result-test').prop('disabled', true)
            }

            if (data_form.name &&
                data_form.phone &&
                data_form.email) {
                $('#send_form_3').prop('disabled', false);
                $('#text_success').text(texts.third)
            } else {
                $('#send_form_3').prop('disabled', true)
            }
            break;
    }
}

$(document).ready(function () {
    let $phone = $('.input__phone');

    $phone.mask('+7 (000) 000-0000');

    $phone.on("click", function () {
        if ($(this).val().length == 0) {
            $(this).val('+7');
        }
    });

    $('.form_name').change(function () {
        data_form.name = $(this).val();
        $('#form_name_result').text(data_form.name);
        checkForm();
    });

    $('.form_city').change(function () {
        data_form.city = $(this).val();
        checkForm();
    });

    $('.form_phone').change(function () {
        if ($(this).val().length != 17) {
            $(this).addClass('input__error');
        } else {
            $(this).removeClass('input__error');
            data_form.phone = $(this).val();
        }
        checkForm();
    });

    $('.form_email').change(function () {
        if (!validateEmail($(this).val())) {
            $(this).addClass('input__error');
        } else {
            $(this).removeClass('input__error');
            data_form.email = $(this).val();
        }
        checkForm();
    });

    $('.form-checkbox input').change(function () {
        if ($(this).prop('checked')) {
            data_form.links.push($(this).val());
        } else {
            data_form.links.remove($(this).val())
        }
        checkForm();
    })

    $('.form-radio input').change(function () {
        let name = $(this).prop('name'),
            value = $(this).val();

        data_form[name] = value;
        checkForm();
    })

    $('.close').click(function () {
        $('.tab').each(function () {
            $('.tab-link .ring').removeClass('ring_gold');
            $(this).removeClass('tab-open').css('display', 'none');
        });

        $('.main').removeClass('main-open');
        $('.left').removeClass('left-open');
    });

    $('#send_form').click(function () {
        $('.tab').each(function () {
            $(this).removeClass('tab-open').css('display', 'none');
        });
        $('.tab-3').addClass('tab-open');
        $('.tab-3').show();
        $("html,body").animate({scrollTop: 0}, 300);
    });

    $('#send_form_2').click(function () {
        $('.tab').each(function () {
            $(this).removeClass('tab-open').css('display', 'none');
        });
        $('.tab-3').addClass('tab-open');
        $('.tab-3').show();
        $("html,body").animate({scrollTop: 0}, 300);
    });

    $('#test').click(function () {
        $('.tab').each(function () {
            $(this).removeClass('tab-open').css('display', 'none');
        });
        $('.tab-4').addClass('tab-open');
        $('.tab-4').show();
        $("html,body").animate({scrollTop: 0}, 300);
    });

    $('#get-result-test').click(function () {

        if (texts.test_answer_number % 2 == 0) {
            $('.tab-5 .test__title').text(texts.test_answer_1);
        } else {
            $('.tab-5 .test__title').text(texts.test_answer_2);
        }
        texts.test_answer_number++;

        $('.tab').each(function () {
            $(this).removeClass('tab-open').css('display', 'none');
        });
        $('.tab-5').addClass('tab-open');
        $('.tab-5').show();
        $("html,body").animate({scrollTop: 0}, 300);
    });

    $('#send_form_3').click(function () {
        $('.tab').each(function () {
            $(this).removeClass('tab-open').css('display', 'none');
        });
        $('.tab-3').addClass('tab-open');
        $('.tab-3').show();
        $("html,body").animate({scrollTop: 0}, 300);
    });

    $('#main-video').click(function () {
        $('.popup-video').css('display', 'block');
    });

    $('.close-video').click(function () {
        $('.popup-video').css('display', 'none');
    });

    $(".range-inv").slider({
        animate: "slow",
        range: "min",
        value: 1500000,
        step: 10,
        min: 350000,
        max: 3000000,
        slide: function (event, ui) {
            let num = ui.value.toString().replace(
                /\B(?=(\d{3})+(?!\d))/g, " ");
            $('.price-inv__total').text(num);
            data_form.sum = num;
        }
    });
});